#!/bin/bash
killall xcape 2>/dev/null
setxkbmap -option >/dev/null


setxkbmap us -variant colemak -option ctrl:nocaps -option ‘caps:ctrl_modifier’ -option shift:both_capslock && xmodmap ~/.Xmodmap
xcape -e 'Control_L=Escape;Hyper_L=Tab;Hyper_R=backslash;Caps_Lock=Escape'
